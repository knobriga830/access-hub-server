package org.accesshub.server.commands;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

public abstract class BaseCommand<T> {

	@TargetAggregateIdentifier
	public final T id;

	public BaseCommand(T id) {
		this.id = id;
	}
}
