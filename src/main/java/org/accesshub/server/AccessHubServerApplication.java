package org.accesshub.server;

import org.accesshub.server.config.EventConfigProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({EventConfigProperties.class})
public class AccessHubServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccessHubServerApplication.class, args);
	}

}
