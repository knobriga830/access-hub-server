package org.accesshub.server.services;

import org.accesshub.server.aggregates.UserProfileAggregate;
import org.accesshub.server.events.BaseEvent;
import org.accesshub.server.events.enums.ProfileStatus;
import org.accesshub.server.models.UserProfile;
import org.accesshub.server.querys.FindUserProfileQuery;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.axonframework.messaging.Message;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserProfileQueryService {

	private final EventStore eventStore;

	private EventSourcingRepository<UserProfileAggregate> userProfileAggregateEventSourcingRepository;

	private HashMap<String, UserProfile> userProfiles = new HashMap<>();

	public UserProfileQueryService(EventStore eventStore, EventSourcingRepository<UserProfileAggregate> userProfileAggregateEventSourcingRepository) {
		this.eventStore = eventStore;
		this.userProfileAggregateEventSourcingRepository = userProfileAggregateEventSourcingRepository;
	}

	public List<Object> getEvents(String username) {
		return eventStore.readEvents(username).asStream().map(Message::getPayload).collect(Collectors.toList());
	}

	@QueryHandler
	public UserProfile getUserProfile(FindUserProfileQuery findUserProfileQuery) {
		if (userProfiles.containsKey(findUserProfileQuery.getUsername())) {
			return userProfiles.get(findUserProfileQuery.getUsername());
		}
		throw new EntityNotFoundException("User Profile not found");
	}


	@EventSourcingHandler
	void handle(BaseEvent event) {
		persistUserProfile(updateUserProfile(getUserProfileFromEvent(event)));
	}

	private void persistUserProfile(UserProfile userProfile) {
		userProfiles.put(userProfile.getUsername(), userProfile);
	}

	private UserProfile updateUserProfile(UserProfileAggregate userProfileFromEvent) {
		UserProfile existingOrNewUserProfile = findExistingOrCreateUserProfile(userProfileFromEvent.getUsername());
		existingOrNewUserProfile.setFirstName(userProfileFromEvent.getFirstName());
		existingOrNewUserProfile.setLastName(userProfileFromEvent.getLastName());
		existingOrNewUserProfile.setEmail(userProfileFromEvent.getEmail());
		existingOrNewUserProfile.setStatus(ProfileStatus.valueOf(userProfileFromEvent.getStatus()));
		return existingOrNewUserProfile;
	}

	private UserProfileAggregate getUserProfileFromEvent(BaseEvent event) {
		return userProfileAggregateEventSourcingRepository.load(event.id.toString()).getWrappedAggregate().getAggregateRoot();
	}

	private UserProfile findExistingOrCreateUserProfile(String username) {
		if (userProfiles.containsKey(username))
			return userProfiles.get(username);
		else {
			UserProfile newUserProfile = new UserProfile();
			newUserProfile.setUsername(username);
			return newUserProfile;
		}
	}
}
