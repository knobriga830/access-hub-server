package org.accesshub.server.events.enums;

public enum ProfileStatus {
	ENABLED,
	DISABLED,
	DELETED
}
