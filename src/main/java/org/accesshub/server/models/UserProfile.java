package org.accesshub.server.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.accesshub.server.events.enums.ProfileStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfile {

	private String username;
	private String firstName;
	private String lastName;
	private String email;
	private ProfileStatus status;

}
