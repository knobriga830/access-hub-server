package org.accesshub.server.aggregates;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.accesshub.server.commands.CreateUserProfileCommand;
import org.accesshub.server.commands.EditUserProfileCommand;
import org.accesshub.server.events.*;
import org.accesshub.server.events.enums.ProfileStatus;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Data
@NoArgsConstructor
@Aggregate(snapshotTriggerDefinition = "thresholdTriggerDefinition")
public class UserProfileAggregate {

	@AggregateIdentifier
	private String username;

	private String firstName;

	private String lastName;

	private String email;

	private String status;

	@CommandHandler
	public UserProfileAggregate(CreateUserProfileCommand createUserProfileCommand) {
		AggregateLifecycle.apply(new UserProfileCreatedEvent(createUserProfileCommand.id, createUserProfileCommand.getFirstName(), createUserProfileCommand.getLastName(), createUserProfileCommand.getEmail()));
	}

	@CommandHandler
	protected void on(EditUserProfileCommand editUserProfileCommand) {
		UserProfileChangedEvent profileChangedEvent = new UserProfileChangedEvent(editUserProfileCommand.id, editUserProfileCommand.getFirstName(), editUserProfileCommand.getLastName(), editUserProfileCommand.getEmail());
		AggregateLifecycle.apply(profileChangedEvent);
	}

	@EventSourcingHandler
	protected void handle(UserProfileCreatedEvent userProfileCreatedEvent) {
		this.username = userProfileCreatedEvent.id;
		this.firstName = userProfileCreatedEvent.getFirstName();
		this.lastName = userProfileCreatedEvent.getLastName();
		this.email = userProfileCreatedEvent.getEmail();

		//ACTIVATE PROFILE
		AggregateLifecycle.apply(new UserProfileActivatedEvent(this.username, ProfileStatus.ENABLED));
	}

	@EventSourcingHandler
	protected void handle(UserProfileChangedEvent userProfileChangedEvent) {
		this.firstName = userProfileChangedEvent.getFirstName();
		this.lastName = userProfileChangedEvent.getLastName();
		if (!this.email.equals(userProfileChangedEvent.getEmail())) {
			UserProfileEmailChangedEvent emailChangedEvent = new UserProfileEmailChangedEvent(this.username, userProfileChangedEvent.getEmail());
			AggregateLifecycle.apply(emailChangedEvent);
		}
	}

	@EventSourcingHandler
	protected void handle(UserProfileEmailChangedEvent emailChangedEvent) {
		this.email = emailChangedEvent.getEmail();
	}

	@EventSourcingHandler
	protected void handle(UserProfileActivatedEvent userProfileActivatedEvent) {
		this.status = String.valueOf(userProfileActivatedEvent.getStatus());
	}

	@EventSourcingHandler
	protected void handle(UserProfileDisabledEvent userProfileDisabledEvent) {
		this.status = String.valueOf(userProfileDisabledEvent.getStatus());
	}

}
