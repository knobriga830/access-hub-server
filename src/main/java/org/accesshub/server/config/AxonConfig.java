package org.accesshub.server.config;

import org.accesshub.server.aggregates.UserProfileAggregate;
import org.axonframework.config.EventProcessingConfigurer;
import org.axonframework.eventhandling.tokenstore.inmemory.InMemoryTokenStore;
import org.axonframework.eventsourcing.*;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AxonConfig {

	@Bean
	EventSourcingRepository<UserProfileAggregate> userProfileAggregateEventSourcingRepository(EventStore eventStore, SnapshotTriggerDefinition thresholdTriggerDefinition) {
		EventSourcingRepository.Builder<UserProfileAggregate> builder = EventSourcingRepository.builder(UserProfileAggregate.class);
		builder.eventStore(eventStore);
		builder.snapshotTriggerDefinition(thresholdTriggerDefinition);
		return builder.build();
	}

	@Bean
	public AggregateSnapshotter snapshotter(EventStore eventStore, @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") AggregateFactory<UserProfileAggregate> userProfileAggregateFactory) {
		AggregateSnapshotter.Builder builder = new AggregateSnapshotter.Builder();
		builder.eventStore(eventStore);
		builder.aggregateFactories(userProfileAggregateFactory);

		return builder.build();
	}


	@Autowired
	public void configureInMemoryTokenStore(EventProcessingConfigurer configurer) {
		configurer.registerTokenStore(configuration -> new InMemoryTokenStore());
	}

	@Bean
	public SnapshotTriggerDefinition thresholdTriggerDefinition(Snapshotter snapshotter, EventConfigProperties eventConfigProperties) {
		return new EventCountSnapshotTriggerDefinition(snapshotter, eventConfigProperties.getSnapshotThreshold());
	}
}
